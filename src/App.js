import React from 'react';
import Counter from './components/Counter';
import Header from './components/Header'
import Clock from './components/Clock'
import ControlledVsUncontrolled from './components/ControlledVsUncontrolled';
//import ReactDOM from 'react-dom';

//funkcijska komponenta koja rendera JSX
function App() {
  return (
    <div className="App">
      <Header title="this is my counter"/>
      <Counter />
      <Clock />
      <ControlledVsUncontrolled />
      
    </div>
  );
}

// const headingStyle = {
//   color: 'red',
//   backgroundColor : 'green'
// }

export default App;
