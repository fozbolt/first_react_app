import React, { useState } from 'react'

function ControlledVsUncontrolled() {
        // you can have multiple useState hooks, e.g. for each individual property
        const [fName, setUserData] = useState({ firstName: ""});
        const [lName, setUserData] = useState({ lastName: "" });
      
        function handleChange(event) {
           //razdvaja objekt ako maknemo spread i userData, ako maknemo samo spread prebriše objekt 
           //primjer : the resulting state object will just be { cats: 'no' }. The new value is not merged into the old one, it is just replaced. If you want to maintain the whole state object, you would want to use the spread operator:
           //kada koristimo spread vraca nam orignalan objekt s updejtanim promjenama (ne zamjenjuje/prebrisuje vrijednosti)
           //setUserData({ ...userData, [event.target.name]: event.target.value });
           //setUserData({ userData, [event.target.name]: event.target.value });
           setUserData({  [event.target.name]: event.target.value });
           console.log(fName, lName)
        }
      
        return (
          <div>
            <br/><br/><br/><br/><br/>
            <h4>Task with controlled and uncontrolled events:</h4><br/>
            <div>Welcome, {userData.firstName + " " + userData.lastName}</div>
            <div>
              <label>First name:</label>
              <input
                /* dolazi do type conversiona iz undefined u string i baca controlled-uncontrolled conversion error */
                value={fName ||""}
                /* or defaultValue={userData.firstName} */
                name="firstName"
                onChange={handleChange}
              />
            </div>
            <div>
              <label>Last name:</label>
              <input
                value={lName || ""}
                name="lastName"
                onChange={handleChange}
              />
            </div>
         </div>
        );
      }

export default ControlledVsUncontrolled
