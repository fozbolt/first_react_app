import React from 'react'
import ReactDOM from 'react-dom';



class Clock extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          date: new Date(),
          name: undefined
        };
    }
  
    componentDidMount() {
      this.timerID = setInterval(
        () => this.tick(),
        1000
      );
    }
  
    componentWillUnmount() {
      clearInterval(this.timerID);
    }
  
    tick() {
      this.setState({
        date: new Date(),
      });  
    }

    // radi samo s arrow funkcijom zbog konteksta
    setName = event =>{
      this.setState({name : event.target.value})
      }

    //isto radi samo s arrow funkcijom
    // getName = () => {
    //   console.log(this.state.name)
    // }

    //ovo bi nekako trebalo biti onchange a ne da se refresha svake sekunde - za to bi vj trebalo razdvojiti komponente
    getTimeOfTheDay = () => {
      let hours = this.state.date.getHours()
      let timeOfDay = undefined

      if (hours>= 8 && hours <12) timeOfDay = 'morning'
      else if (hours>= 12 && hours <18) timeOfDay = 'afternoon'
      else if (hours>= 18 && hours <24) timeOfDay = 'evening'
      else if (hours>= 0 && hours <8) timeOfDay = 'night'

      return timeOfDay
    }



    render() {
      return (
        <div>
          <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
          <input placeholder='Enter name' onChange={this.setName} /> <br/>
          {/* <button onClick ={this.getName}> Show name </button> */}

          
          {
            /* izvrsava se samo ako smo unijeli ime */
            this.state.name !== undefined && this.state.name !== '' &&
              <label>
                Good {this.getTimeOfTheDay()}, {this.state.name}
              </label>
          }
         
        </div>
      );
    }
  }
  
  ReactDOM.render(
    <Clock />,
    document.getElementById('root')
  );

  export default Clock