import React from 'react'
import ReactDOM from 'react-dom';

class Counter extends React.Component {
    constructor(props) {
      super(props);
      this.state = {counter: 0};
    }
  
    componentDidMount() {
      this.timerID = setInterval(
        () => this.tick(),
        5000
      );
    }
  
    componentWillUnmount() {
      clearInterval(this.timerID);
    }
  
    tick() {
      this.setState({
        counter : this.state.counter + 1
      });  
    }
  
    render() {
      return (
        <div>
          <h2>Counter value: {this.state.counter}</h2>
        </div>
      );
    }
  }
  
  ReactDOM.render(
    <Counter />,
    document.getElementById('root')
  );

export default Counter
