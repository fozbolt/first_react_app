//rafce naredba za automatski  generira boiler plate komponente
import React from 'react'

const Header = (props) => {
    return (
        <header>
            <h1>Hello, {props.title}</h1>
        </header>
   )
}

Header.defaultProps = {
    title: 'Default title'
}

export default Header
